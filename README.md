# varrank: an R package for multi-targets variables selection

(PUBLIC) !!! UNSTABLE VERSION !!!

varrank is a one man show (me!) and made of more than 1000 lines of code which are not bug free! So use it with caution and awareness.

## Installation

`install.packages("https://git.math.uzh.ch/gkratz/varrank/raw/master/varrank_0.2.tar.gz", repo=NULL, type="source")`

CRAN: https://CRAN.R-project.org/package=varrank
Website: https://www.math.uzh.ch/pages/varrank/

## Description

A computational toolbox of heuristics approaches for performing variable ranking and feature selection based on mutual information well adapted for multivariate system epidemiology datasets. The core function is a general implementation of the minimum redundancy maximum relevance model. R. Battiti (1994) <doi:10.1109/72.298224>. Continuous variables are discretized using a large choice of rule. Variables ranking can be learned with a sequential forward/backward search algorithm. The two main problems that can be addressed by this package is the selection of the most representative variable within a group of variables of interest (i.e. dimension reduction) and variable ranking with respect to a set of features of interest.

## Future implementations (ordered by urgency)
